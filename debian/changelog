wmsystemtray (1.4+git20150508-3) UNRELEASED; urgency=medium

  * Update Vcs-* after migration to Salsa.
  * Update Format in d/copyright to https.
  * Switch Priority from extra to optional.
  * Bump Standards-Version to 4.1.3.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 10 Jan 2018 14:46:53 -0500

wmsystemtray (1.4+git20150508-2) unstable; urgency=medium

  * debian/control
    - Update Vcs-* fields.
    - Set Debian Window Maker Team as Maintainer; move myself to
      Uploaders with updated email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 23 Aug 2015 23:58:26 -0400

wmsystemtray (1.4+git20150508-1) unstable; urgency=medium

  * New upstream release (git snapshot).
    - Date in manpage is no longer generated at build time.  This allows
      reproducible builds.
  * debian/{control,copyright}
    - Tidy up using wrap-and-sort.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Thu, 14 May 2015 09:12:37 -0500

wmsystemtray (1.4-3) unstable; urgency=medium

  * debian/control
    - Bump Standards-Version to 3.9.6; no changes required.
    - Replace Build-Depends on autotools-dev with dh-autoreconf.
  * debian/Makefile.*
    - Remove files; left over from when upstream included debian/ in source
      tarball.
  * debian/rules
    - Use dh_autoreconf.
    - Add get-orig-source target.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Fri, 28 Nov 2014 14:51:36 -0600

wmsystemtray (1.4-2) unstable; urgency=low

  * debian/control
    - Add Vcs-* fields
  * debian/rules
    - Add export V=1 to debian/rules so that compiler flags are not hidden.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Mon, 21 Jul 2014 01:33:59 -0500

wmsystemtray (1.4-1) unstable; urgency=low

  * New upstream version.
  * debian/control
    - Bump Standards-Version to 3.9.5, no changes required.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Sun, 16 Mar 2014 08:17:37 -0500

wmsystemtray (1.2-1) unstable; urgency=low

  * Initial release. (Closes: #714769)

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Fri, 20 Sep 2013 18:23:13 -0500
